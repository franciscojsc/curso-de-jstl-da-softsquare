create database jstl;

use database jstl;

create table clientes(
	id int primary key auto_increment,
    nome varchar(55),
    idade int
);
insert into clientes(nome, idade) values('Francisco', 24);
insert into clientes(nome, idade) values('Nize', 19 );
insert into clientes(nome, idade) values('Leandro',84 );
insert into clientes(nome, idade) values('Gato', 1);
insert into clientes(nome, idade) values('Sonia', 45);
insert into clientes(nome, idade) values('Paulo',26 );
insert into clientes(nome, idade) values('Carlos',58 );
insert into clientes(nome, idade) values('Flavia',14 );
insert into clientes(nome, idade) values('Danila',76 );
insert into clientes(nome, idade) values('Josefa',35 );
insert into clientes(nome, idade) values('Alex',21 );

select * from clientes;