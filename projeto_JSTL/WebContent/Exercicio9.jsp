<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio ForEach 9 JSTL</title>
	</head>
	
	<body>
	
		<c:forEach var="i" begin="0" end="10">
			<c:out value="${i}"></c:out>
		</c:forEach>
		
		
		<!-- exemplo com select para peguar valores e inserir, dentro do select -->
		<select>
			<c:forEach var="i" begin="0" end="10">
				<option>
					<c:out value="${i}"></c:out>
				</option>
			</c:forEach>	
		</select>
	</body>
</html>