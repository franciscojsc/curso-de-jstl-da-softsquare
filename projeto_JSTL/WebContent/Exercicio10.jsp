<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio url 10 JSTL</title>
	</head>
	
	<body>
	
	<!-- armazenar valores na url, para poder ser passado junto  -->
		<c:url value="index.jsp" scope="session" var="meuparamento">
			<c:param name="produto" value="computador"></c:param>
			<c:param name="preco" value="1.2000.00"></c:param>
			<c:param name="id" value="4456"></c:param>
		</c:url>
		
		<c:import url="${meuparamento}"></c:import>
		
	</body>
</html>