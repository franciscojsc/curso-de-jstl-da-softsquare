<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   <!-- Taglib begin -->
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

    <!-- Taglib end -->
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio Functions containsIgnoreCase 23 JSTL</title>
	</head>
	
	<body>
	
		<c:set var="string" value="Second: I am a good programmer in java"></c:set>
		
		<c:if test="${fn:contains(string,'java') }">
			<c:out value="Sim contém java"></c:out>
		</c:if>
		
		<c:if test="${fn:contains(string,'JAVA') }">
			<c:out value="Sim contém java"></c:out>
		</c:if>
		<c:if test="${fn:containsIgnoreCase(string,'java') }">
			<c:out value="Sim contém java"></c:out>
		</c:if>
		
		<c:if test="${fn:containsIgnoreCase(string,'JAVA') }">
			<c:out value="Sim contém java"></c:out>
		</c:if>
		
		<c:if test="${!fn:containsIgnoreCase(string,'java') }">
			<c:out value="Não contém java"></c:out>
		</c:if>
		
		
		
		
	</body>
</html>