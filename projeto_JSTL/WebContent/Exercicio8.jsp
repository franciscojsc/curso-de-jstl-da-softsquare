<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio choose 8 JSTL</title>
	</head>
	
	<body>
	
		<c:set var="valor1" value="${300}"></c:set>
		<c:set var="valor2" value="${200}"></c:set>
	
	<!-- Semelhante ao switch case -->	
		<c:choose >
			<c:when test="${valor1 >= valor2 }">
				<c:out value="é maior ou igual"></c:out>
			</c:when>
			<c:when test="${valor1 <= valor2 }">
				<c:out value="é menor ou igual"></c:out>
			</c:when>
			<c:otherwise>
				<c:out value="nem maior  men menor e nem igual"></c:out>
			</c:otherwise>
		</c:choose>	
	</body>
</html>