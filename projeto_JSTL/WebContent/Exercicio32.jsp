<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="java.util.*" %>

   <!-- Taglib begin -->
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

    <!-- Taglib end -->
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio sql insert 32 JSTL</title>
	</head>
	
	<body>
	
		<sql:setDataSource var="snapshot" 
		url="jdbc:mysql://localhost:33063/jstl" 
		driver="com.mysql.jdbc.Driver"
		 user="root" password="1234" />
		 
		 <c:catch var="capturaExcecaoInsert">
			 <sql:update dataSource="${snapshot}" var="count">
			 		insert into clientes(nome,idade) values ('bozolino',  65);
			 </sql:update>
		</c:catch>	
		<c:if test="${ not empty capturaExcecaoInsert }">
			<c:out value="${capturaExcecaoInsert }"></c:out>
		</c:if>
		
			
		<c:catch var="capturaExcecao">
			<sql:query dataSource="${snapshot}" var="result" >
				select * from clientes;
			</sql:query>	
		</c:catch>	
		<c:if test="${ not empty capturaExcecao }">
			<c:out value="${capturaExcecao }"></c:out>
		</c:if>
		
		<table border="1">
			<c:forEach var="rows" items="${result.rows}" >
				<tr style="font-size='25px';">
			
					<td><c:out value="${rows.id}"></c:out><br/></td>
					<td><c:out value="${rows.nome}"></c:out><br/></td>
					<td><c:out value="${rows.idade}"></c:out><br/></td>
				
				</tr>
			</c:forEach>
		</table>
	</body>
</html>