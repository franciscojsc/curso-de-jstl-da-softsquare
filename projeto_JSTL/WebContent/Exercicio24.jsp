<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   <!-- import da classe Date -->
<%@ page import="java.util.Date" %>

   <!-- Taglib begin -->
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

    <!-- Taglib end -->
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio Format 24 JSTL</title>
	</head>
	
	<body>
	
		<c:set var="agora" value="<%= new java.util.Date() %>"/>
		
		Data formatada : ---- 1  ----
		<f:formatDate type="time" value="${agora}"   />
		
		<br/>
		
		Data formatada : ---- 2  ----
		<f:formatDate type="both" value="${agora}"  />
		<br/>
		
		Data formatada : ---- 3  ----
		<f:formatDate type="date" value="${agora}"  />
		
		<br/>
		
		Data formatada : ---- 4  ----
		<f:formatDate type="both" dateStyle="Long" timeStyle="Long" value="${agora}"  />
		
		<br/>
		Data formatada : ---- 5  ----
		<f:formatDate type="both" dateStyle="medium" timeStyle="medium" value="${agora}"  />
		
		<br/>
		Data formatada : ---- 6  ----
		<f:formatDate type="both" dateStyle="short" timeStyle="short" value="${agora}"  />
		
		<br/>
		
		
		<!-- utilizando a tag jsp useBean para criar uma variavel do tipo Date -->
		
		<jsp:useBean id='now' class='java.util.Date'/>
		Data formatada : ---- 7  ----
		<f:formatDate value="${now}"  pattern="dd/MM/yyyy HH:mm:ss" timeZone="GMT-03:00" />
		
		<br/>
		
		Data formatada : ---- 8  ----
		<f:formatDate value="${now}"  pattern="dd-MM-yyyy" />
		
	</body>
</html>