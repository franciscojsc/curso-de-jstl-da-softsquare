<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title> Exercicio Tag remove 3 JSTL</title>
	</head>
	
	<body>
		
		
		
		<c:set scope="session" var="valor" value="${100*10}"></c:set>
	
	<br/>Valor setado :	<c:out  value="${valor}"></c:out>
	
	<c:remove var="valor"/>	
	<br/>Valor setado apos remove :	<c:out value="${valor}"></c:out>
		
	</body>
</html>