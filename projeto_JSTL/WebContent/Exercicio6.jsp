<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio If 6 JSTL</title>
	</head>
	
	<body>
	
		<c:set var="valor" value="100"></c:set>
	
		<c:if test="${valor > 10}">
			<c:out value="é maior que 10"></c:out>
		</c:if>
	
		<c:if test="${valor < 10}">
			<c:redirect url="index.jsp"></c:redirect>
		</c:if>
	
	
	</body>
</html>