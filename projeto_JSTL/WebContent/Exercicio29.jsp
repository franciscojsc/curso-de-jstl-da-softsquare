<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

   <!-- Taglib begin -->
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

    <!-- Taglib end -->
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio fmt Request Encoding 29 JSTL</title>
	</head>
	
	<body>
	
		
		Português:<br/>
		<f:requestEncoding value="UTF-8"/>
		
		<f:setBundle basename="br.com.projeto.Exemplo_BR" var="Localidade"/>
		
			<f:message key="count.one" bundle="${Localidade }" /> <br/>
			<f:message key="count.two" bundle="${Localidade }"/> <br/>
			<f:message key="count.three" bundle="${Localidade }"/> <br/>
			
		Inglês:<br/>
		
		<f:setBundle basename="br.com.projeto.Exemplo_US" var="Localidade"/>
		
			<f:message key="count.one" bundle="${Localidade }" /> <br/>
			<f:message key="count.two" bundle="${Localidade }"/> <br/>
			<f:message key="count.three" bundle="${Localidade }"/> <br/>
			
	 
		
	</body>
</html>