<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   <!-- Taglib begin -->
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

    <!-- Taglib end -->
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio Functions join split 19 JSTL</title>
	</head>
	
	<body>
	
	<!--
	 A função JSTL Join é usada para concatenar as seqüências contidas
	 em uma matriz com a seqüência especificada. 
	 "Fn: join" é uma função string da JSP Standard Tag Library (JSTL). 
	 Esta função é usada para retornar string com todos os elementos 
	 de uma matriz separados por um "separador". 
	 A função fn: join () concatena todos os elementos de um array em uma
	  string com um separador especificado. Sintaxe: 
	  A função fn: join () tem a seguinte sintaxe:
	  String join (java.lang.String[], java.lang.String)
	 -->
	
		<c:set var="string1" value="I am a good programmer in java"></c:set>
		<c:set var="array1" value="${fn:split(string1, ' ')}"></c:set>
  		<c:out value="${fn:join(array1, '-')}"></c:out>
		
	
		
	</body>
</html>