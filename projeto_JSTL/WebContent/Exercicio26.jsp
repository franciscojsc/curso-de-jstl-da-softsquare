<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   <!-- import da classe Date -->
<%@ page import="java.util.Date" %>

   <!-- Taglib begin -->
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

    <!-- Taglib end -->
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio fmt FormatNumber 26 JSTL</title>
	</head>
	
	<body>
	
		<c:set var="valor" value="12004500000"/>
		
		valor antes :<c:out value="${valor}"></c:out> <br/>
		
		valor formatado currency: <f:formatNumber type="currency" value="${valor}"></f:formatNumber> <br/>
		valor formatado number: <f:formatNumber type="number" value="${valor}"></f:formatNumber> <br/>
		valor formatado percent: <f:formatNumber type="percent" value="${valor}"></f:formatNumber> <br/>
		
		
		valor formatado n maxIntegerDigits: <f:formatNumber type="number" maxIntegerDigits="6" value="${valor}"></f:formatNumber> <br/>
		valor formatado n maxFractionDigits: <f:formatNumber type="number" maxFractionDigits="3" value="${valor}"></f:formatNumber> <br/>
		valor formatado n groupingUsed: <f:formatNumber type="number" groupingUsed="false" value="${valor}"></f:formatNumber> <br/>
		
		valor formatado p maxIntegerDigits: <f:formatNumber type="percent" maxIntegerDigits="6" value="${valor}"></f:formatNumber> <br/>
		valor formatado p maxFractionDigits: <f:formatNumber type="percent" maxFractionDigits="3" value="${valor}"></f:formatNumber> <br/>
		valor formatado p groupingUsed: <f:formatNumber type="percent" groupingUsed="false" value="${valor}"></f:formatNumber> <br/>
		
		valor formatado currency masacara : <f:formatNumber type="currency" pattern="###.###E0" value="${valor}"></f:formatNumber> <br/>

		<f:setLocale value="en_US"/>
		valor formatado currency : USA SET LOCALE <f:formatNumber type="currency" value="${valor}"></f:formatNumber> <br/>
		
		
	</body>
</html>