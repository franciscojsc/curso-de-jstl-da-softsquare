<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Setar Variaveis Exercicio 2 JSTL</title>
	</head>
	
	<body>
		
		<c:set var="texto" value="exercicio de jstl"></c:set>
	
		<c:out value="${texto}"></c:out>
		
		<c:set var="soma" value="${10*10}"></c:set>
	
		<c:out value="${soma}"></c:out>
	
		
	</body>
</html>