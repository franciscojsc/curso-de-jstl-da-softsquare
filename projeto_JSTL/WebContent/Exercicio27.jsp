<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

   <!-- Taglib begin -->
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

    <!-- Taglib end -->
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Exercicio fmt SET LOCALE  27 JSTL</title>
	</head>
	
	<body>
	
		Inglês:<br/>
		<f:bundle basename="br.com.projeto.Exemplo_US">
			<f:message key="count.one"></f:message> <br/>
			<f:message key="count.two"></f:message> <br/>
			<f:message key="count.three"></f:message> <br/>
		</f:bundle>
		
		Português:<br/>
		<f:setLocale value="pt_BR"/>
		
		<f:bundle basename="br.com.projeto.Exemplo_BR">
			<f:message key="count.one"></f:message> <br/>
			<f:message key="count.two"></f:message> <br/>
			<f:message key="count.three"></f:message> <br/>
		</f:bundle>
		
	   Espanhol:<br/>
		<f:setLocale value="es_ES"/>
		
		<f:bundle basename="br.com.projeto.Exemplo_ES">
			<f:message key="count.one"></f:message> <br/>
			<f:message key="count.two"></f:message> <br/>
			<f:message key="count.three"></f:message> <br/>
		</f:bundle>
		
	</body>
</html>