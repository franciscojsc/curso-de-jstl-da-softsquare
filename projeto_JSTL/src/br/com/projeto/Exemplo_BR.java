package br.com.projeto;

import java.util.ListResourceBundle;

public class Exemplo_BR extends ListResourceBundle{

	@Override
	protected Object[][] getContents() {
		
		return contents;
	}
	
	static final Object[] [] contents = {
			{"count.one","Um"},	
			{"count.two","Dois"},	
			{"count.three","Tres"},	
	};

}
