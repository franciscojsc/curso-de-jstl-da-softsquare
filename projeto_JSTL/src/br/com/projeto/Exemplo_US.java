package br.com.projeto;

import java.util.ListResourceBundle;

public class Exemplo_US extends ListResourceBundle{

	@Override
	protected Object[][] getContents() {
		
		return contents;
	}
	
	static final Object[] [] contents = {
			{"count.one","One"},	
			{"count.two","Two"},	
			{"count.three","Three"},	
	};

}
