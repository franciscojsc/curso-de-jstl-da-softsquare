package br.com.projeto;

import java.util.ListResourceBundle;

public class Exemplo_ES extends ListResourceBundle{

	@Override
	protected Object[][] getContents() {
		
		return contents;
	}
	
	static final Object[] [] contents = {
			{"count.one","uno"},	
			{"count.two","Dos"},	
			{"count.three","Tres"},	
	};

}
